#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <openmpi/mpi.h>

// Define string to unsigned 64 bit integer
#define atoui64(x) strtoull(x, NULL, 10)

// Define the root process id
#define ROOT 0

// Define a PRNG to initialize an other PRNG
// with longer period to provide better randomness,
// the init function implements a Middle Weyl PRNG.
uint64_t init(uint64_t seed) {
    static uint64_t x = 0, w = 0;
    w += seed;
    x *= x;
    x += w;
    x = (x >> 32) | (x << 32);
    return x;
}

// Define state vector size
#define VSIZE 2

// A uniform distribution is implemented using a PRNG
// with long periodicity. In this case a Xor-Shift 128+
// is used, with period 2^128 represented by an array of
// two unsigned 64 bits integers.
double uniform(uint64_t *v) {
    uint64_t a = v[0];
    const uint64_t b = v[1];
    const uint64_t out = a + b;
    v[0] = b;
    a ^= a << 23;
    v[1] = a ^ b ^ (a >> 18) ^ (b >> 5);
    return (double)out / UINT64_MAX;
}

// The Xor-Shift 128+ PRNG use a state vector in
// order to perform the pseudo-random number generation.
// It is possible to apply a transformation to this matrix
// skipping a fixed range of numbers.

static const uint64_t ALPHA[] = {0x8A5CD789635D2DFF, 0x121FD2155C472F96};

void skip(uint64_t *v) {
    uint64_t a = 0, b = 0;

    for (int i = 0; i < VSIZE; i++)
        for (int bit = 0; bit < 64; bit++) {
            if (ALPHA[i] & 1UL << bit) { a ^= v[0]; b ^= v[1]; }
            uniform(v);
        }

    v[0] = a; v[1] = b;
}

// The following function represents the Probability Density
// Function of a Gamma distribution. This is the target function
// of a Monte Carlo random sampling method.
double gamma_pdf(double x, double alpha, double beta) {
    double out = 1;
    out *= pow(beta, -alpha);
    out *= pow(x, alpha - 1);
    out *= exp(-x / beta);
    for (double i = 2; i < alpha - 1; i++) out /= i;
    return out;
}

int main(int argc, char **argv) {
    double start, stop, wtime, procwtime;
    int rank, size, coords[2];

    // Initialize MPI
    MPI_Init(&argc, &argv);

    // Set start event time
    start = MPI_Wtime();

    // Get process rank and communicator size
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Create the grid communicator
    MPI_Comm grid;
    int dims[2], periods[2];

    dims[0] = (int) floor(sqrt((double) size));
    dims[1] = (int) round((double) size / dims[0]);

    periods[0] = false;
    periods[1] = false;

    MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, false, &grid);

    // Update communicator size and get proc coords
    MPI_Comm_size(grid, &size);
    MPI_Cart_coords(grid, rank, 2, coords);

    // Declare simulation variables
    double x, y;
    uint64_t samples, v[VSIZE], *V_MATRIX;

    int alpha, beta;
    double X;
    
    double sendbuf[2], recvbuf[2];
    for (int i = 0; i < 2; i++) sendbuf[i] = 0;

    // If process rank is zero, use this process as root
    if (rank == ROOT) {
        alpha = atoi(argv[1]);
        beta = atoi(argv[2]);
        X = atof(argv[3]);
        
        // Set the number of sample per process
        samples = atoui64(argv[4]);
        samples = (uint64_t) ceil((double) samples / size);

        // Initialize a state vector
        uint64_t seed = (uint64_t) clock();
        V_MATRIX = (uint64_t *) malloc(sizeof(uint64_t) * VSIZE * dims[0]);
        for (int i = 0; i < VSIZE * dims[0]; i++) V_MATRIX[i] = init(seed);
    }

    // Scatterv data across grid
    int *counts = (int *) malloc(sizeof(int) * VSIZE * size);
    int *displs = (int *) malloc(sizeof(int) * VSIZE * size);

    for (int i = 0; i < size; i++) {
        counts[i] = VSIZE;
        displs[i] = VSIZE * (i % dims[1]);
    }

    MPI_Scatterv(V_MATRIX, counts, displs, MPI_UINT64_T, v, VSIZE, MPI_UINT64_T, ROOT, grid);

    // Broadcast Monte-Carlo Gamma PDF parameters
    MPI_Bcast(&alpha, 1, MPI_INT, ROOT, grid);
    MPI_Bcast(&beta, 1, MPI_INT, ROOT, grid);
    MPI_Bcast(&X, 1, MPI_DOUBLE, ROOT, grid);
    MPI_Bcast(&samples, 1, MPI_UINT64_T, ROOT, grid);

    // Execute the Skip function using coords
    for (int i = 0; i < coords[1]; i++) skip(v);
    
    // Compute the number of samples associated with this process
    for (uint64_t i = 0; i < samples; i++) {
        x = uniform(v) * 256;
        y = uniform(v);
        if (gamma_pdf(x, alpha, beta) > y) {
            if (x < X) sendbuf[1]++;
            sendbuf[0]++;
        }
    }

    // Reduce results
    MPI_Reduce(sendbuf, recvbuf, 2, MPI_DOUBLE, MPI_SUM, ROOT, grid);

    stop = MPI_Wtime();
    wtime = stop-start;
    MPI_Reduce(&wtime, &procwtime, 1, MPI_DOUBLE, MPI_MAX, ROOT, grid);

    if (rank == ROOT) {
        printf(
            "Estimated P(X < %.0f) with X ~ Gamma(%d, %d): %f\n",
            X, alpha, beta, recvbuf[1]/recvbuf[0]
        );
        printf("Estimated wall clock time using MPI_Wtime: %.4f\n", procwtime);
    }

    MPI_Finalize();
    return 0;
}
