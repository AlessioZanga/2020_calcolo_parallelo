#include <float.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cuda.h>

// Define string to unsigned 64 bit integer
#define atoui64(x) strtoull(x, NULL, 10)

// Define a PRNG to initialize an other PRNG
// with longer period to provide better randomness,
// the init function implements a Middle Weyl PRNG.
uint64_t init(uint64_t seed) {
	static uint64_t x = 0, w = 0;
	w += seed;
	x *= x;
	x += w;
	x = (x >> 32) | (x << 32);
	return x;
}

// Define state vector size
#define VSIZE 2

// Define read-only dynamic memory (texture)
texture<unsigned int> V_MATRIX;

// Define constant memory
__constant__ uint64_t ALPHA[2];

// A uniform distribution is implemented using a PRNG
// with long periodicity. In this case a Xor-Shift 128+
// is used, with period 2^128 represented by an array of
// two unsigned 64 bits integers.
__device__ float uniform(uint64_t *v) {
	uint64_t a = v[0];
	const uint64_t b = v[1];
	uint64_t out = a + b;
	v[0] = b;
	a ^= a << 23;
	v[1] = a ^ b ^ (a >> 18) ^ (b >> 5);
	return (float) out / UINT64_MAX;
}

// The Xor-Shift 128+ PRNG use a state vector in
// order to perform the pseudo-random number generation.
// It is possible to apply a transformation to this matrix
// skipping a fixed range of numbers.

__device__ void skip(uint64_t *v) {
	uint64_t a = 0, b = 0;

	for (int i = 0; i < VSIZE; i++)
		for (int bit = 0; bit < 64; bit++) {
			if (ALPHA[i] & 1UL << bit) {
				a ^= v[0];
				b ^= v[1];
			}
			uniform(v);
		}

	v[0] = a;
	v[1] = b;
}

// The following function represents the Probability Density
// Function of a Gamma distribution. This is the target function
// of a Monte Carlo random sampling method.
__device__ float gamma_pdf(float x, float alpha, float beta) {
	float out = 1;
	out *= pow(beta, -alpha);
	out *= pow(x, alpha - 1);
	out *= exp(-x / beta);
	for (int i = 2; i < alpha - 1; i++)
		out /= i;
	return out;
}

__global__ void generate_samples(int alpha, int beta, float X, int samples,
		int *results, int N) {
	float x, y;
	uint64_t v[VSIZE];
	int count = 0, accept = 0;

	// Define shared cache for sum reduction over block
	extern __shared__ int reduce[];

	// Compute thread id
	int tid = threadIdx.x;

	if (tid < N) {
		// Get the state vector associated
		// to this specific CUDA block
		for (int i = 0; i < VSIZE; i++) {
			v[i] = tex1Dfetch(V_MATRIX, VSIZE * blockIdx.x + i);
		}

		// Apply the skip function to the local transformation
		// matrix to compute the matrix associated with this thread
		for (int i = 0; i < tid; i++) skip(v);

		// Compute samples using local state vector
		for (int i = 0; i < samples; i++) {
			x = uniform(v) * 256;
			y = uniform(v);
			if (gamma_pdf(x, alpha, beta) > y) {
				if (x < X)
					count++;
				accept++;
			}
		}

		// Share local computation
		reduce[2 * tid] = count;
		reduce[2 * tid + 1] = accept;
	}

	// Synchronize threads
	__syncthreads();

	if (tid == 0) {
		// Reduce the block counted
		count = 0;
		accept = 0;
		
		for (int i = 0; i < N; i++) {
			count += reduce[2 * i];
			accept += reduce[2 * i + 1];
		}

		atomicAdd(&results[0], count);
		atomicAdd(&results[1], accept);
	}
}

int main(int argc, char **argv) {
	cudaEvent_t start, stop;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);

	uint64_t A[] = { 0x8A5CD789635D2DFF, 0x121FD2155C472F96 };
	uint64_t *v, *dev_v;
	int *results, *dev_results;

	int alpha, beta;
	float X;

	alpha = atoi(argv[1]);
	beta = atoi(argv[2]);
	X = atof(argv[3]);

	int samples, threads, blocks;
	uint64_t samplesui64 = atoui64(argv[4]);
	samples = (int) ceil(pow((double) samplesui64, 0.5));
	threads = (int) ceil(pow((double) samplesui64, 0.3));
	blocks  = (int) ceil(pow((double) samplesui64, 0.2));

	// Initialize state vector
	uint64_t seed = (uint64_t) clock();
	v = (uint64_t *) malloc(sizeof(uint64_t) * VSIZE * blocks);
	for (int i = 0; i < VSIZE * blocks; i++) v[i] = init(seed);
	cudaMalloc(&dev_v, sizeof(uint64_t) * VSIZE * blocks);

	results = (int *) malloc(sizeof(int) * 2);
	cudaMalloc(&dev_results, sizeof(int) * 2);
	cudaMemset(dev_results, 0, sizeof(int) * 2);

	cudaBindTexture(NULL, V_MATRIX, dev_v, sizeof(uint64_t) * VSIZE * blocks);
	cudaMemcpy(dev_v, v, sizeof(uint64_t) * VSIZE * blocks, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(ALPHA, A, sizeof(uint64_t) * 2);

	// KERNEL
	generate_samples<<<blocks, threads, (sizeof(int) * 2 * threads)>>>(alpha,
			beta, X, samples, dev_results, threads);

	cudaMemcpy(results, dev_results, sizeof(int) * 2, cudaMemcpyDeviceToHost);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	float elapsedTime;
	cudaEventElapsedTime(&elapsedTime, start, stop);

	printf("Estimated P(X < %.0f) with X ~ Gamma(%d, %d): %f\n", X, alpha, beta,
			(double) results[0] / results[1]);
	printf("Estimated wall clock time using cudaEvent: %.4f\n", elapsedTime / 1000);

	cudaUnbindTexture(V_MATRIX);
	cudaFree(dev_v);
	cudaFree(dev_results);

	return 0;
}
