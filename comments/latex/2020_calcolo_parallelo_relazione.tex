\documentclass[tikz,12pt,oneside,a4paper]{book}
\usepackage[a4paper,left=3cm,right=2cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath,amssymb}
\usepackage{mathtools}
\usepackage{fancyvrb}
\usepackage{float}
\usepackage{xcolor}

\usepackage{hyperref}
\hypersetup{
	colorlinks,
	citecolor=black,
	filecolor=black,
	linkcolor=black,
	urlcolor=black
}

\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}
	\author{Alessio Zanga}
	\title{Approssimazione della Funzione di Ripartizione}
	
	\maketitle
	\tableofcontents
	\begingroup
	\let\clearpage\relax
	\listoffigures
	\listoftables
	\endgroup
	
	\chapter{Funzioni di Probabilità}
	
		\section{Densità di Probabilità e di Probabilità Cumulata}
		Una Funzione di Densità di Probabilità $f$ (Probability Density Function - PDF) di una
		variabile aleatoria $X$ continua descrive densità di probabilità in ogni punto dello spazio campionario. \\
		
		Una Funzione di Ripartizione $F$ (Cumulative Distribution Function - CDF) è una funzione che associa ad un valore $x$ la probabilità che la variabile aleatoria $X$ assuma valori minori o uguali ad $x$:
		\begin{equation}
			F(x) = P(X \leq x) = \int_{-\infty}^{x} f(u) d(u)
		\end{equation}
		
		\noindent La rappresentazione grafica di una CDF consiste in:
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.4]{../img/PDF_CDF.png}
			\caption{Rappresentazione di una CDF}
		\end{figure}
		
		\section{Stima della CDF tramite Acceptance-Rejection}
		Non tutte le Funzioni di Densità di Probabilità sono facilmente integrabili o dispongono di un integrale in forma chiusa. La stima del valore di una CDF per un dato valore $x$ viene effettuata tramite il metodo Acceptance-Rejection. \\
		
		Sia $f(x)$ la PDF associata ad una variabile aleatoria $X$ definita in un intervallo chiuso e limitato $[a, b]$ con codominio $[0, c]$. Generando due sequenze pseudo-casuali uniformi $U_1$ e $U_2$, campionando da ciascuna di esse una serie di valori, è possibile formare delle coppie in forma $(U_1, U_2)$. Ciascuna di queste coppie corrisponde ad una coppia $(X, Y)$ nell'intervallo $[a, b] \times [0, c]$. Se la coppia $(U_1, U_2)$ ricade nell'area delimitata da $f(x)$ allora viene accettata, altrimenti viene rigettata.
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.3]{../img/acceptance.png}
			\caption{Metodo di Acceptance-Rejection applicato ad una generica PDF.}
		\end{figure}
		
		Per stimare una CDF si generano dei campioni che appartengono a $P(X)$ (che indicheremo con il termine ``accepted'') e successivamente si osserva se i campioni così generati appartengono anche a $P(X \leq x)$ per una dato $x$ (che indicheremo con ``counted''). \\
		
		La stima di $P(X \leq x)$ è data dal rapporto tra il numero di campioni accepted e counted. Questo metodo è generico e permette di	stimare funzioni non direttamente integrabili, ma	richiede un	numero considerevole di campioni.
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.4]{../img/gamma_2_3_7_E5.png}
			\includegraphics[scale=0.4]{../img/gamma_2_3_7_E6.png}
			\includegraphics[scale=0.4]{../img/gamma_2_3_7_E7.png}
			\caption{Metodo Acceptance-Rejection con distribuzione $Gamma(2, 3)$ per $X \leq 7$. I campioni $counted$ sono evidenziali in blu, mentre i campioni $accepted$ sono dati dall'unione dei campioni in blu e dei campioni in azzurro. Il numero di campioni è rispettivamente $10^5, 10^6$ e $10^7$.}
			\label{fig:gamma_2_3}
		\end{figure}
	
	\chapter{Generazione di Numeri Pseudo-Casuali}
	
		\section{Pseudo-Random Number Generators - PRNG}
		Al fine di generare un numero sufficiente di campioni di che siano considerabili pseudo-casuali, è necessario implementare un generatore con un periodo sufficientemente ampio. \\
		
		XorShift 128 utilizza un vettore $v$ formato da due unsigned int da 64 bit (o quattro da 32 bit) per simulare una sequenza pseudo-casuale con periodo $2^{128}$. Questo vettore viene detto ``stato corrente''. L'effort di parallelizzazione si concentra sulla generazione di una sequenza sfruttando le proprietà dello stato corrente e delle operazioni di xor e shift.
		
		\section{Parallelizzazione Naïve}
		Un primo approccio naïve per parallelizzare questo algoritmo consiste nell'assegnare una
		frazione dei campioni da generare ad ogni processo.  \\
		
		Dato che lo stato del generatore è interamente costituito dal vettore di stato corrente, è sufficiente generare $2n$ unsigned int differenti ed assegnarne due ad ogni processo per poter utilizzare correttamente lo XorShift 128.
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.6]{../img/multiple_generators.png}
			\caption{Parallelizzazione naïve con generatori multipli.}
		\end{figure}
		
		Il problema di questo approccio è dato dalla possibile sovrapposizione delle sequenze: generatori con seed diversi non generano necessariamente sequenze diverse.
			
		\section{Transformation Matrix - Tecnica di Skip}
		Un secondo approccio alla parallelizzazione	consiste nello sfruttare le caratteristiche del	vettore di stato $v$. Ad ogni numero generato, il vettore viene	aggiornato secondo una serie di trasformazioni	che possono essere riassunte in:
		\begin{equation}
			v_{i+1} = v_i T
		\end{equation}
		con $T$ matrice di trasformazione che riassume teoricamente in un prodotto matrice-vettore le operazioni di xor e shift. \\
		
		\noindent Questo processo viene ripetuto ad ogni estrazione di un nuovo campione:
		\begin{equation}
			vT \rightarrow vT^2 \rightarrow vT^3 \rightarrow \dots \rightarrow vT^n
		\end{equation}
		Dato che la matrice $T$ è costante, è possibile calcolare la distanza $|j-i|$ tra uno stato j-esimo ed uno stato i-esimo precedente con un vettore costante $\alpha_i$:
		\begin{equation}
			vT^j = \sum \alpha_i v T^i
		\end{equation}
		con $\alpha_i$ detta costante di ``skip'' (o ``jump'') di periodo $i$. \\
		
		\noindent Ad ogni $\alpha_i$ il generatore effettua uno skip in avanti di un numero di campioni pari a $2^i$. La tecnica di skip permette di evitare che due generatori con lo stesso seed (o lo	stesso generatore in parallelo) si sovrappongano per un intervallo pari a $2^i$.
		
		\section{Combinazione delle Tecniche precedenti}
		Combinando la tecnica di skip con più generatori in parallelo si riduce ulteriormente la
		probabilità che le sequenze si sovrappongano. Di fatto, invece che avere $n$ generatori in parallelo, si hanno $k$ generatori ciascuno con $h$ versioni con skip.
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.6]{../img/multiple_generators_with_skip.png}
			\caption{Parallelizzazione combinata con generatori multipli e tecnica di skip.}
		\end{figure}
	
	\chapter{Implementazione in C}
	
		\section{Implementazione MPI}
		L'implementazione in MPI prevede di sfruttare la struttura su due livelli indotta dalla combinazione delle tecniche. \\
		
		Definito un processo ROOT che si occupa di inizializzare i seed dei generatori, la disposizione dei generatori segue una $\textit{griglia bidimensionale}$ che indicheremo con $grid$, dove per ogni cella $(i,j)$:
		\begin{itemize}
			\item Ogni riga è associata ad un particolare vettore di stato $v_i$.
			\item Ogni colonna è associata ad un numero di skip pari a $j$.
		\end{itemize}
	
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.4]{../img/MPI_grid.png}
			\caption{Griglia bidimensionale utilizzata nell'implementazione con MPI.}
			\label{fig:mpi_grid}
		\end{figure}
		
		\noindent La costruzione di $grid$ avviene tramite l'utilizzo di $\verb|MPI_Cart_create|$. Le dimensioni della griglia vengono determinate in modo che le righe siano in funzione della radice quadrata dei processi, approssimando per difetto, e successivamente le colonne sono il rapporto tra il numero di processi e le righe. Ciò permette di adattare le dimensioni anche a un numero di processi che non sono un quadrato esatto (e.g. $\floor{\sqrt{15}} = 3$ righe, $15/3 = 5$ colonne). \\
		
		Ad esempio, nella figura \ref{fig:mpi_grid} il processo con rank $7$ è associato al vettore $v_1$ ed effettua $3$ skip prima di generare dei numeri. \\
		
		La combinazione delle due tecniche permette di generare un $O(k)$ seed invece che $O(n)$, riducendo i tempi di inizializzazione a carico del processo ROOT. L'invio dei vettori $v_i$ associati ai seed avviene tramite l'utilizzo di $\verb|MPI_Scatterv|$, che nel caso di XorShift 128 si occupa di inviare due unsigned int ad ogni processo nella griglia. I valori del vettore di displacement dipendono solo dal numero di riga, in modo che ogni riga riceva lo stesso $v_i$. \\
		
		Una volta inviati i seed, viene effettuato una $\verb|MPI_Bcast|$ dei parametri necessari per il calcolo della PDF. Dopo aver generato i campioni necessari è sufficiente utilizzare una $\verb|MPI_Reduce|$ su un vettore di dimensione due predisposto per raccogliere $accepted$ e $counted$, ottenendo così i dati per completare la stima. \\
		
		Con questa implementazione si riduce al minimo la comunicazione necessaria per inizializzare correttamente i generatori in parallelo, così da ottenere una minore probabilità di sovrapposizione e quindi una buona stima della funzione da approssimare.
		
		\section{Implementazione OpenMP}
		L'implementazione in OpenMP massimizza il numero di vettori di stato da inizializzare sfruttando il fatto che l'accesso ad essi avviene in sola lettura da parte di ogni threads. \\
		
		Il ciclo $for$ di generazione dei campioni è l'obiettivo principale della parallelizzazione. Ogni thread mantiene un proprio vettore $v_i$ privata tramite una copia delle locazioni necessarie dalla variabile $\verb|V_MATRIX|$ shared. Effettuare una copia locale del vettore $v_i$ elimina eventuali cali prestazionali dovuti all'accesso concorrente a $\verb|V_MATRIX|$, riducendo il tempo di esecuzione. \\
		
		Il calcolo dei campioni procede in parallelo per ogni thread e si conclude con una doppia reduce in forma $\verb|reduction(+:counted) reduction(+:accepted)|$.
		
		\section{Implementazione CUDA}
		L'implementazione in CUDA fa corrispondere alla struttura hardware della GPU la struttura combinata delle due tecniche:
		\begin{itemize}
			\item Ogni blocco è associato ad un vettore di stato $v_i$.
			\item Ogni thread effettua uno skip pari all'id del thread stesso.
		\end{itemize}
	
		In questo modo è possibile inizializzare un numero di seed pari al numero di blocchi, minimizzando la quantità di memoria da trasferire da host a device. \\
		
		I numero di blocchi, threads e campioni per thread vengono determinati in funzione del numero di campioni totali tramite una serie di radici n-esime. Sia $x$ il numero di campioni totali, allora dati tre coefficienti $a, b$ e $c$ tali che:
		\begin{equation}
			x = \underbrace{x^a}_{samples} \cdot \underbrace{x^b}_{threads} \cdot \underbrace{x^c}_{blocks} \qquad \text{ con } a,b,c \in [0,1] \subseteq \mathbb{R}
		\end{equation}
		si associa ad ogni termine del prodotto rispettivamente il numero di blocchi, threads e campioni per thread. \\
		
		Con una serie di esecuzioni sperimentali si osserva che la tripla $(0.5, 0.3, 0.2)$ costituisce un buon metodo per bilanciare il carico di lavoro al variare del numero di campioni. \\
		
		La reduce dei risultati viene effettuata in due fasi. Nella prima fase viene utilizzata un'area di memoria shared a livello di blocco per sommare i risultati parziali dei thread. Successivamente si sincronizzano i threads di ogni blocco e si utilizza un thread per blocco per effettuare una $addAtomic$ ad una variabile per completare la reduce tra i blocchi. \\
		
		Il tipo di memoria da associare ad ogni variabile è stato scelto in funzione delle dimensioni e dell'utilizzo della variabile stessa:
		\begin{itemize}
			\item $\verb|ALPHA|$ - Dato che il vettore costante $\alpha$ ha dimensioni note a priori è possibile utilizzare la $\verb|__constant__ memory|$ come tipo di memoria costante.
			\item $\verb|V_MATRIX|$ - Dato che ogni thread di ogni blocco accede a questo vettore in sola lettura, ma la dimensione del vettore non è nota a priori, si deve utilizzare la $\verb|texture memory|$ come tipo di memoria costante.
			\item $\verb|reduce|$ - Dato che il numero di thread è dinamico, è necessario definire un'area di $\verb|dynamic shared memory|$ su cui effettuare la prima fase di reduce.
		\end{itemize}
	
	\chapter{Risultati sperimentali}
	
		\section{Informazioni preliminari}
		Le misurazioni sperimentali sono state effettuate su una macchina con tale configurazione:
		\begin{itemize}
			\item CPU: AMD ThreadRipper 1900X 8C@16T
			\item GPU: NVIDIA GeForce RTX 2070 8GB GDDR6
			\item RAM: 32GB DDR4 2993MHz ECC
		\end{itemize}
	
		\noindent Il metodo di stima del tempo di esecuzione di ogni implementazione è il seguente:
		\begin{itemize}
			\item Implementazione sequenziale - Differenza tra due istanti temporali misurati tramite l'utility $\verb|clock|$ fornita dalla libreria $\verb|time.h|$ di C.
			\item Implementazione MPI - Utilizzo della funzione $\verb|MPI_Wtime|$ per ottenere tempo di inizio e tempo di fine di ogni processo in combinazione con una $\verb|MPI_Reduce|$ per calcolare il wall-clock time.
			\item Implementazione OpenMP - Differenza tra due istanti temporali misurati con la funzione $\verb|omp_get_wtime|$ messa a disposizione da OpenMP.
			\item Implementazione CUDA - Differenza tra due eventi registrati tramite le funzioni $\verb|cudaEventCreate|$, $\verb|cudaEventRecord|$ e $\verb|cudaEventElapsedTime|$.
		\end{itemize}
	
		\noindent La funzione PDF da stimare utilizzata è associata ad una distribuzione Gamma di parametri $\alpha = 2$ e $\beta = 3$ per $P(X \leq 7) = 0.676760$. Tale scelta è motivata dal fatto che la distribuzione in oggetto possiede un andamento in grado di formare bordi sinusoidali che costituiscono il caso limite per i campioni da stimare, come visto in figura $\ref{fig:gamma_2_3}$. \\
		
		Ogni misurazione è stata ripetuta dieci volte e ne è stata calcolata il valor medio e la deviazione standard. Il range di campioni totali varia da $10^5$ a $10^{10}$.
	
		\newpage
		
		\subsection{Risultati sperimentali in Forma tabellare}
		I risultati per le implementazioni in MPI e OpenMP sono riportati in funzione del numero di cores/threads utilizzati.
		\begin{table}[H]
			\centering
			\begin{tabular}{|c|c|r|r|r|r|}
				\hline
				\textbf{runner}    & \multicolumn{1}{c|}{\textbf{samples}} & \multicolumn{1}{c|}{\textbf{result\_avg}} & \multicolumn{1}{c|}{\textbf{result\_std}} & \multicolumn{1}{c|}{\textbf{time\_avg}} & \multicolumn{1}{c|}{\textbf{time\_std}} \\ \hline
				\textbf{serial}    & $10^{5}$                                & 0.682445                                  & 0.023112                                  & 0.0133                                  & 0.0026                                  \\ \hline
				\textbf{serial}    & $10^{6}$                               & 0.676252                                  & 0.008207                                  & 0.0926                                  & 0.0033                                  \\ \hline
				\textbf{serial}    & $10^{7}$                              & 0.676893                                  & 0.002239                                  & 0.8859                                  & 0.0088                                  \\ \hline
				\textbf{serial}    & $10^{8}$                             & 0.676493                                  & 0.000847                                  & 8.7511                                  & 0.0763                                  \\ \hline
				\textbf{serial}    & $10^{9}$                            & 0.676732                                  & 0.000207                                  & 86.1618                                 & 0.8056                                  \\ \hline
				\textbf{serial}    & $10^{10}$                           & 0.676734                                  & 0.000075                                  & 857.0700                                & 2.3442                                  \\ \hline
			\end{tabular}
			\caption{Risultati implementazione sequenziale in forma tabellare.}
		\end{table}
		
		\begin{table}[H]
			\centering
			\begin{tabular}{|c|c|r|r|r|r|}
				\hline
				\textbf{runner}    & \multicolumn{1}{c|}{\textbf{samples}} & \multicolumn{1}{c|}{\textbf{result\_avg}} & \multicolumn{1}{c|}{\textbf{result\_std}} & \multicolumn{1}{c|}{\textbf{time\_avg}} & \multicolumn{1}{c|}{\textbf{time\_std}} \\ \hline
				\textbf{MPI-4}     & $10^{5}$                                & 0.684562                                  & 0.031444                                  & 0.0048                                  & 0.0004                                  \\ \hline
				\textbf{MPI-4}     & $10^{6}$                               & 0.679218                                  & 0.012796                                  & 0.0305                                  & 0.0040                                  \\ \hline
				\textbf{MPI-4}     & $10^{7}$                              & 0.677147                                  & 0.003438                                  & 0.2504                                  & 0.0174                                  \\ \hline
				\textbf{MPI-4}     & $10^{8}$                             & 0.676770                                  & 0.000664                                  & 2.2645                                  & 0.0299                                  \\ \hline
				\textbf{MPI-4}     & $10^{9}$                            & 0.676871                                  & 0.000168                                  & 22.4577                                 & 0.1362                                  \\ \hline
				\textbf{MPI-4}     & $10^{10}$                           & 0.676771                                  & 0.000119                                  & 223.4626                                & 0.8012                                  \\ \hline
				\textbf{MPI-8}     & $10^{5}$                                & 0.637578                                  & 0.024906                                  & 0.0044                                  & 0.0031                                  \\ \hline
				\textbf{MPI-8}     & $10^{6}$                               & 0.673282                                  & 0.007695                                  & 0.0227                                  & 0.0100                                  \\ \hline
				\textbf{MPI-8}     & $10^{7}$                              & 0.675730                                  & 0.002538                                  & 0.1617                                  & 0.0064                                  \\ \hline
				\textbf{MPI-8}     & $10^{8}$                             & 0.675801                                  & 0.001355                                  & 1.2247                                  & 0.0632                                  \\ \hline
				\textbf{MPI-8}     & $10^{9}$                            & 0.676889                                  & 0.000307                                  & 11.5970                                 & 0.1531                                  \\ \hline
				\textbf{MPI-8}     & $10^{10}$                           & 0.676788                                  & 0.000108                                  & 114.6038                                & 0.4356                                  \\ \hline
				\textbf{MPI-16}    & $10^{5}$                                & 0.679892                                  & 0.042230                                  & 0.0149                                  & 0.0110                                  \\ \hline
				\textbf{MPI-16}    & $10^{6}$                               & 0.674197                                  & 0.024437                                  & 0.0217                                  & 0.0158                                  \\ \hline
				\textbf{MPI-16}    & $10^{7}$                              & 0.675759                                  & 0.004860                                  & 0.0987                                  & 0.0237                                  \\ \hline
				\textbf{MPI-16}    & $10^{8}$                             & 0.676189                                  & 0.001134                                  & 0.8931                                  & 0.0776                                  \\ \hline
				\textbf{MPI-16}    & $10^{9}$                            & 0.676704                                  & 0.000489                                  & 8.5939                                  & 0.1159                                  \\ \hline
				\textbf{MPI-16}    & $10^{10}$                           & 0.676667                                  & 0.000122                                  & 84.7720                                 & 0.3518                                  \\ \hline
			\end{tabular}
			\caption{Risultati implementazione MPI in forma tabellare.}
		\end{table}
		
		\begin{table}[H]
			\centering
			\begin{tabular}{|c|c|r|r|r|r|}
			\hline
			\textbf{runner}    & \multicolumn{1}{c|}{\textbf{samples}} & \multicolumn{1}{c|}{\textbf{result\_avg}} & \multicolumn{1}{c|}{\textbf{result\_std}} & \multicolumn{1}{c|}{\textbf{time\_avg}} & \multicolumn{1}{c|}{\textbf{time\_std}} \\ \hline
				\textbf{OpenMP-4}  & $10^{5}$                                & 0.676080                                  & 0.033997                                  & 0.0045                                  & 0.0003                                  \\ \hline
				\textbf{OpenMP-4}  & $10^{6}$                               & 0.678556                                  & 0.007128                                  & 0.0303                                  & 0.0015                                  \\ \hline
				\textbf{OpenMP-4}  & $10^{7}$                              & 0.675602                                  & 0.001878                                  & 0.2398                                  & 0.0108                                  \\ \hline
				\textbf{OpenMP-4}  & $10^{8}$                             & 0.676585                                  & 0.000615                                  & 2.2324                                  & 0.0130                                  \\ \hline
				\textbf{OpenMP-4}  & $10^{9}$                            & 0.676789                                  & 0.000148                                  & 22.2209                                 & 0.0836                                  \\ \hline
				\textbf{OpenMP-4}  & $10^{10}$                           & 0.676804                                  & 0.000095                                  & 222.5309                                & 0.1998                                  \\ \hline
				\textbf{OpenMP-8}  & $10^{5}$                                & 0.684245                                  & 0.017993                                  & 0.0029                                  & 0.0005                                  \\ \hline
				\textbf{OpenMP-8}  & $10^{6}$                               & 0.676220                                  & 0.005651                                  & 0.0200                                  & 0.0017                                  \\ \hline
				\textbf{OpenMP-8}  & $10^{7}$                              & 0.675975                                  & 0.002096                                  & 0.1438                                  & 0.0090                                  \\ \hline
				\textbf{OpenMP-8}  & $10^{8}$                             & 0.676715                                  & 0.000797                                  & 1.2412                                  & 0.0789                                  \\ \hline
				\textbf{OpenMP-8}  & $10^{9}$                            & 0.676580                                  & 0.000306                                  & 11.5053                                 & 0.1737                                  \\ \hline
				\textbf{OpenMP-8}  & $10^{10}$                           & 0.676786                                  & 0.000045                                  & 114.5150                                & 0.5691                                  \\ \hline
				\textbf{OpenMP-16} & $10^{5}$                                & 0.683583                                  & 0.023066                                  & 0.0030                                  & 0.0003                                  \\ \hline
				\textbf{OpenMP-16} & $10^{6}$                               & 0.677445                                  & 0.009268                                  & 0.0133                                  & 0.0018                                  \\ \hline
				\textbf{OpenMP-16} & $10^{7}$                              & 0.676716                                  & 0.001533                                  & 0.0844                                  & 0.0062                                  \\ \hline
				\textbf{OpenMP-16} & $10^{8}$                             & 0.676492                                  & 0.000684                                  & 0.8302                                  & 0.0414                                  \\ \hline
				\textbf{OpenMP-16} & $10^{9}$                            & 0.676879                                  & 0.000157                                  & 8.1012                                  & 0.1879                                  \\ \hline
				\textbf{OpenMP-16} & $10^{10}$                           & 0.676704                                  & 0.000060                                  & 80.3625                                 & 1.1533                                  \\ \hline
			\end{tabular}
			\caption{Risultati implementazione OpenMP in forma tabellare.}
		\end{table}
	
		\begin{table}[H]
			\centering
			\begin{tabular}{|c|c|r|r|r|r|}
			\hline
			\textbf{runner}    & \multicolumn{1}{c|}{\textbf{samples}} & \multicolumn{1}{c|}{\textbf{result\_avg}} & \multicolumn{1}{c|}{\textbf{result\_std}} & \multicolumn{1}{c|}{\textbf{time\_avg}} & \multicolumn{1}{c|}{\textbf{time\_std}} \\ \hline
				\textbf{CUDA}      & $10^{5}$                                & 0.690252                                  & 0.025669                                  & 0.0005                                  & 0.0000                                  \\ \hline
				\textbf{CUDA}      & $10^{6}$                               & 0.679443                                  & 0.006324                                  & 0.0011                                  & 0.0000                                  \\ \hline
				\textbf{CUDA}      & $10^{7}$                              & 0.677507                                  & 0.002349                                  & 0.0029                                  & 0.0002                                  \\ \hline
				\textbf{CUDA}      & $10^{8}$                             & 0.676862                                  & 0.000561                                  & 0.0117                                  & 0.0018                                  \\ \hline
				\textbf{CUDA}      & $10^{9}$                            & 0.676686                                  & 0.000196                                  & 0.0488                                  & 0.0020                                  \\ \hline
				\textbf{CUDA}      & $10^{10}$                           & 0.676786                                  & 0.000044                                  & 0.4177                                  & 0.0025                                  \\ \hline
			\end{tabular}
			\caption{Risultati implementazione CUDA in forma tabellare.}
		\end{table}
		
		\newpage
		
		\section{Tempo vs. Numero di Campioni}
		Di seguito sono riportate le misurazioni di tempo vs. numero di campioni totali.
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.31]{../img/lines_time_avg_samples_all.png}
			\includegraphics[scale=0.31]{../img/lines_time_std_samples_all.png}
			\caption{Tempo medio e deviazione standard rispetto al numero totale di campioni impiegato dalle implementazioni senza restrizioni.}
			\label{fig:time_avg_samples_all}
		\end{figure}
	
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.31]{../img/lines_time_avg_samples_MPI.png}
			\includegraphics[scale=0.31]{../img/lines_time_std_samples_MPI.png}
			\caption{Tempo medio e deviazione standard rispetto al numero totale di campioni impiegato dall'implementazione MPI al variare del numero di processi.}
		\end{figure}

		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.31]{../img/lines_time_avg_samples_OpenMP.png}
			\includegraphics[scale=0.31]{../img/lines_time_std_samples_OpenMP.png}
			\caption{Tempo medio e deviazione standard rispetto al numero totale di campioni impiegato dall'implementazione in OpenMP al variare del numero di processi.}
		\end{figure}
	
		\newpage
		
		\section{Errore Relativo vs. Numero di Campioni}
		Di seguito sono riportate le misurazioni di errore relativo vs. numero di campioni totali.
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.31]{../img/lines_rel_err_samples_all.png}
			\includegraphics[scale=0.31]{../img/lines_result_std_samples_all.png}
			\caption{Errore relativo e deviazione standard rispetto al numero totale di campioni impiegato dalle implementazioni senza restrizioni.}
		\end{figure}
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.31]{../img/lines_rel_err_samples_MPI.png}
			\includegraphics[scale=0.31]{../img/lines_result_std_samples_MPI.png}
			\caption{Errore relative e deviazione standard rispetto al numero totale di campioni impiegato dall'implementazione MPI al variare del numero di processi.}
		\end{figure}
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.31]{../img/lines_rel_err_samples_OpenMP.png}
			\includegraphics[scale=0.31]{../img/lines_result_std_samples_OpenMP.png}
			\caption{Errore relativo e deviazione standard rispetto al numero totale di campioni impiegato dall'implementazione in OpenMP al variare del numero di processi.}
		\end{figure}
		
		\newpage
		
		\section{Errore Relativo vs. Tempo}
		Di seguito sono riportate le misurazioni di errore relativo vs. tempo.
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.4]{../img/lines_rel_err_time_avg_all.png}
			\caption{Errore relativo rispetto al tempo medio impiegato dalle implementazioni senza restrizioni.}
			\label{fig:rel_err_time_avg_all}
		\end{figure}
		
		\begin{figure}[H]
			\centering
			\includegraphics[scale=0.3]{../img/lines_rel_err_time_avg_MPI.png}
			\includegraphics[scale=0.3]{../img/lines_rel_err_time_avg_OpenMP.png}
			\caption{Errore relativo rispetto al tempo medio impiegato dall'implementazione MPI e OpenMP al variare del numero di processi.}
		\end{figure}
	
	\chapter{Conclusioni}
	
		\section{Considerazioni finali}
		Nella figura \ref{fig:time_avg_samples_all}, il tempo medio impiegato dalle implementazioni MPI e OpenMP è pressoché lo stesso a parità di cores/threads utilizzati.\\
		
		Un aspetto interessante è dato dal fatto che in MPI presenta un marcato trend di diminuzione dell'errore relativo all'aumentare del numero di campioni indipendentemente dal numero di processi, mentre in OpenMP l'errore relativo diminuisce costantemente all'aumentare del numero di campioni solo con un numero di threads pari a 16. Questo può essere causato dalla minor volatilità data dall'implementazione in MPI per il maggior utilizzo della tecnica di skip con uno schema a griglia. \\
		
		Come mostrato in figura \ref{fig:rel_err_time_avg_all}, l'errore relativo e il tempo impiegato dall'implementazione in CUDA è di ordini di grandezza minore rispetto alle altre implementazioni. In particolare, si può notare come OpenMP richieda inizialmente meno tempo di MPI al costo di una maggiore volatilità. \\
		
		Il codice relativo è disponibile su \href{https://gitlab.com/AlessioZanga/2020_calcolo_parallelo}{\color{blue}{GitLab}}.
		
\end{document}
