#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Define string to unsigned 64 bit integer
#define atoui64(x) strtoull(x, NULL, 10)

// Define a PRNG to initialize an other PRNG
// with longer period to provide better randomness,
// the init function implements a Middle Weyl PRNG.
uint64_t init(uint64_t seed) {
    static uint64_t x = 0, w = 0;
    w += seed;
    x *= x;
    x += w;
    x = (x >> 32) | (x << 32);
    return x;
}

// A uniform distribution is implemented using a PRNG
// with long periodicity. In this case a Xor-Shift 128+
// is used, with period 2^128 represented by an array of
// two unsigned 64 bits integers.
double uniform(uint64_t *v) {
    uint64_t a = v[0];
    const uint64_t b = v[1];
    const uint64_t out = a + b;
    v[0] = b;
    a ^= a << 23;
    v[1] = a ^ b ^ (a >> 18) ^ (b >> 5);
    return (double)out / UINT64_MAX;
}

// The following function represents the Probability Density
// Function of a Gamma distribution. This is the target function
// of a Monte Carlo random sampling method.
double gamma_pdf(double x, double alpha, double beta) {
    double out = 1;
    out *= pow(beta, -alpha);
    out *= pow(x, alpha - 1);
    out *= exp(-x / beta);
    for (double i = 2; i < alpha - 1; i++) out /= i;
    return out;
}

int main(int argc, char **argv) {
    clock_t start, stop;

    start = clock();

    double x, y;
    uint64_t samples, v[2];
    double accepted = 0, counted = 0;

    int alpha, beta;
    double X;

    alpha = atoi(argv[1]);
    beta = atoi(argv[2]);

    // Initialize the X value of P(X < value) query
    X = atof(argv[3]);

    // Initialize the samples value
    samples = atoui64(argv[4]);

    // Initialize the state vector
    uint64_t seed = (uint64_t) clock();
    for (int i = 0; i < 2; i++) v[i] = init(seed);

    // Generate a fixed number of samples
    for (uint64_t i = 0; i < samples; i++) {
        // Scale the samples across the Gamma PDF domain
        x = uniform(v) * 256;
        y = uniform(v);
        // If samples are located inside the PDF accept them
        if (gamma_pdf(x, alpha, beta) > y) {
            // If samples are located inside the query count them
            if (x < X) counted++;
            accepted++;
        }
    }

    stop = clock();

    // Print the probability estimation
    printf("Estimated P(X < %.0f) with X ~ Gamma(%d, %d): %f\n", X, alpha, beta, counted/accepted);
    printf("Estimated wall clock time using CLOCK: %.4f\n", (double)(stop - start) / CLOCKS_PER_SEC);

    return 0;
}
