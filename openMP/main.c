#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

// Define string to unsigned 64 bit integer
#define atoui64(x) strtoull(x, NULL, 10)

// Define a PRNG to initialize an other PRNG
// with longer period to provide better randomness,
// the init function implements a Middle Weyl PRNG.
uint64_t init(uint64_t seed) {
    static uint64_t x = 0, w = 0;
    w += seed;
    x *= x;
    x += w;
    x = (x >> 32) | (x << 32);
    return x;
}

// Define state vector size
#define VSIZE 2

// A uniform distribution is implemented using a PRNG
// with long periodicity. In this case a Xor-Shift 128+
// is used, with period 2^128 represented by an array of
// two unsigned 64 bits integers.
double uniform(uint64_t *v) {
    uint64_t a = v[0];
    const uint64_t b = v[1];
    const uint64_t out = a + b;
    v[0] = b;
    a ^= a << 23;
    v[1] = a ^ b ^ (a >> 18) ^ (b >> 5);
    return (double)out / UINT64_MAX;
}

// The Xor-Shift 128+ PRNG use a state vector in
// order to perform the pseudo-random number generation.
// It is possible to apply a transformation to this matrix
// skipping a fixed range of numbers.

static const uint64_t ALPHA[] = {0x8A5CD789635D2DFF, 0x121FD2155C472F96};

void skip(uint64_t *v) {
    uint64_t a = 0, b = 0;

    for (int i = 0; i < VSIZE; i++)
        for (int bit = 0; bit < 64; bit++) {
            if (ALPHA[i] & 1UL << bit) { a ^= v[0]; b ^= v[1]; }
            uniform(v);
        }

    v[0] = a; v[1] = b;
}

// The following function represents the Probability Density
// Function of a Gamma distribution. This is the target function
// of a Monte Carlo random sampling method.
double gamma_pdf(double x, double alpha, double beta) {
    double out = 1;
    out *= pow(beta, -alpha);
    out *= pow(x, alpha - 1);
    out *= exp(-x / beta);
    for (double i = 2; i < alpha - 1; i++) out /= i;
    return out;
}

int main(int argc, char **argv) {
    double start, stop;

    start = omp_get_wtime();

    int procs;
    uint64_t samples, *V_MATRIX;
    double counted = 0, accepted = 0;

    int alpha, beta;
    double X;

    // Get number of processes
    procs = atoi(argv[1]);
    omp_set_num_threads(procs);

    alpha = atoi(argv[2]);
    beta = atoi(argv[3]);
    X = atof(argv[4]);
    samples = (uint64_t) ceil((double) atoui64(argv[5]) / procs);

    // Initialize the state vector
    uint64_t seed = (uint64_t) clock();
    V_MATRIX = (uint64_t *) malloc(sizeof(uint64_t) * VSIZE * procs);
    for (int i = 0; i < VSIZE * procs; i++) V_MATRIX[i] = init(seed);

    #pragma omp parallel reduction(+:counted) reduction(+:accepted)
    {
        // Get local state vector
        uint64_t v[VSIZE];
        for (int i = 0; i < VSIZE; i++) {
            v[i] = V_MATRIX[VSIZE * omp_get_thread_num() + i];
        }        
    
        double x, y;
        // Compute the samples associated with this process
        for (uint64_t i = 0; i < samples; i++) {
            x = uniform(v) * 256;
            y = uniform(v);
            if (gamma_pdf(x, alpha, beta) > y) {
                if (x < X) counted++;
                accepted++;
            }
        }
    }

    stop = omp_get_wtime();

    printf("Estimated P(X < %.0f) with X ~ Gamma(%d, %d): %f\n", X, alpha, beta, counted/accepted);
    printf("Estimated wall clock time using omp_get_wtime: %.4f\n", stop-start);

    return 0;
}
