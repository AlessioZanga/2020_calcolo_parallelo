#!/usr/bin/env python
import pandas as pd
import seaborn as sns; sns.set(); sns.set_context('poster')
import matplotlib.pyplot as plt

def plot(df, colors, **kwargs):
    plt.figure(figsize=(10,6))
    plt.gcf().subplots_adjust(bottom=0.15, left=0.15)
    df = df[df['runner'].isin(kwargs['runners'])]
    pal = {k: v for k,v in colors.items() if k in kwargs['runners']}
    pal = sns.xkcd_palette(pal.values())
    ax = sns.lineplot(**kwargs['plot'], palette=pal, data=df, markers=True, dashes=False)
    ax.set(**kwargs['set'])
    ax.figure.savefig(kwargs['fig'])

if __name__ == '__main__':
    df = pd.read_csv('../results.csv')

    res = 0.676760
    df['rel_err'] = abs(df['result_avg'] - res)/res

    colors = {
        'serial': 'light orange',
        'MPI-4': 'light blue',
        'MPI-8': 'blue',
        'MPI-16': 'dark blue',
        'openMP-4': 'pastel red',
        'openMP-8': 'bright red',
        'openMP-16': 'dark red',
        'CUDA': 'green'
    }

    # sns.palplot(sns.xkcd_palette(colors.values()))
    # plt.show()

    configs = [
        {
            'runners': ['serial', 'MPI-16', 'openMP-16', 'CUDA'],
            'plot' : {
                'x': 'samples',
                'y': 'time_avg',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Time Avg.',
                'title': 'Time Avg. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_time_avg_samples_all.png'
        },
        {
            'runners': ['MPI-4', 'MPI-8', 'MPI-16'],
            'plot' : {
                'x': 'samples',
                'y': 'time_avg',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Time Avg.',
                'title': 'Time Avg. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_time_avg_samples_MPI.png'
        },
        {
            'runners': ['openMP-4', 'openMP-8', 'openMP-16'],
            'plot' : {
                'x': 'samples',
                'y': 'time_avg',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Time Avg.',
                'title': 'Time Avg. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_time_avg_samples_OpenMP.png'
        },
        {
            'runners': ['serial', 'MPI-16', 'openMP-16', 'CUDA'],
            'plot' : {
                'x': 'samples',
                'y': 'result_avg',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Result Avg.',
                'title': 'Result Avg. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_result_avg_samples_all.png'
        },
        {
            'runners': ['MPI-4', 'MPI-8', 'MPI-16'],
            'plot' : {
                'x': 'samples',
                'y': 'result_avg',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Result Avg.',
                'title': 'Result Avg. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_result_avg_samples_MPI.png'
        },
        {
            'runners': ['openMP-4', 'openMP-8', 'openMP-16'],
            'plot' : {
                'x': 'samples',
                'y': 'result_avg',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Result Avg.',
                'title': 'Result Avg. vs. Number of Samples'
            },
          
            'fig': '../comments/img/lines_result_avg_samples_OpenMP.png'
        },
        {
            'runners': ['serial', 'MPI-16', 'openMP-16', 'CUDA'],
            'plot' : {
                'x': 'samples',
                'y': 'time_std',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Time Std.',
                'title': 'Time Std. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_time_std_samples_all.png'
        },
        {
            'runners': ['MPI-4', 'MPI-8', 'MPI-16'],
            'plot' : {
                'x': 'samples',
                'y': 'time_std',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Time Std.',
                'title': 'Time Std. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_time_std_samples_MPI.png'
        },
        {
            'runners': ['openMP-4', 'openMP-8', 'openMP-16'],
            'plot' : {
                'x': 'samples',
                'y': 'time_std',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Time Std.',
                'title': 'Time Std. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_time_std_samples_OpenMP.png'
        },
        {
            'runners': ['serial', 'MPI-16', 'openMP-16', 'CUDA'],
            'plot' : {
                'x': 'samples',
                'y': 'result_std',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Result Std.',
                'title': 'Result Std. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_result_std_samples_all.png'
        },
        {
            'runners': ['MPI-4', 'MPI-8', 'MPI-16'],
            'plot' : {
                'x': 'samples',
                'y': 'result_std',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Result Std.',
                'title': 'Result Std. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_result_std_samples_MPI.png'
        },
        {
            'runners': ['openMP-4', 'openMP-8', 'openMP-16'],
            'plot' : {
                'x': 'samples',
                'y': 'result_std',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Result Std.',
                'title': 'Result Std. vs. Number of Samples'
            },
            'fig': '../comments/img/lines_result_std_samples_OpenMP.png'
        },
        {
            'runners': ['serial', 'MPI-16', 'openMP-16', 'CUDA'],
            'plot' : {
                'x': 'samples',
                'y': 'rel_err',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Relative Error',
                'title': 'Relative Error vs. Number of Samples'
            },
            'fig': '../comments/img/lines_rel_err_samples_all.png'
        },
        {
            'runners': ['MPI-4', 'MPI-8', 'MPI-16'],
            'plot' : {
                'x': 'samples',
                'y': 'rel_err',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Relative Error',
                'title': 'Relative Error vs. Number of Samples'
            },
            'fig': '../comments/img/lines_rel_err_samples_MPI.png'
        },
        {
            'runners': ['openMP-4', 'openMP-8', 'openMP-16'],
            'plot' : {
                'x': 'samples',
                'y': 'rel_err',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Number of Samples',
                'ylabel': 'Relative Error',
                'title': 'Relative Error vs. Number of Samples'
            },
            'fig': '../comments/img/lines_rel_err_samples_OpenMP.png'
        },
        {
            'runners': ['serial', 'MPI-16', 'openMP-16', 'CUDA'],
            'plot' : {
                'x': 'time_avg',
                'y': 'rel_err',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Time Avg.',
                'ylabel': 'Relative Error',
                'title': 'Relative Error vs. Time Avg.'
            },
            'fig': '../comments/img/lines_rel_err_time_avg_all.png'
        },
        {
            'runners': ['MPI-4', 'MPI-8', 'MPI-16'],
            'plot' : {
                'x': 'time_avg',
                'y': 'rel_err',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Time Avg.',
                'ylabel': 'Relative Error',
                'title': 'Relative Error vs. Time Avg.'
            },
            'fig': '../comments/img/lines_rel_err_time_avg_MPI.png'
        },
        {
            'runners': ['openMP-4', 'openMP-8', 'openMP-16'],
            'plot' : {
                'x': 'time_avg',
                'y': 'rel_err',
                'hue': 'runner',
                'style': 'runner'
            },
            'set' : {
                'xscale': 'log',
                'xlabel': 'Time Avg.',
                'ylabel': 'Relative Error',
                'title': 'Relative Error vs. Time Avg.'
            },
            'fig': '../comments/img/lines_rel_err_time_avg_OpenMP.png'
        },
    ]

    for config in configs:
        plot(df, colors, **config)
