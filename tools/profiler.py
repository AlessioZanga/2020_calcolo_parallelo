#!/usr/bin/env python
import subprocess
from itertools import product
from statistics import mean, stdev

RUNNERS = {
    'serial': '../serial/main.out',
    'MPI-4': 'mpirun -n 4 ../MPI/main.out',
    'MPI-8': 'mpirun -n 8 ../MPI/main.out',
    'MPI-16': 'mpirun -n 16 ../MPI/main.out',
    'openMP-4': '../openMP/main.out 4',
    'openMP-8': '../openMP/main.out 8',
    'openMP-16': '../openMP/main.out 16',
    'CUDA': '../CUDA/Release/CUDA',
}

def get_profiles():
    args = [
        RUNNERS.values(),      # Programs
        ['2'],    # Alpha parameters
        ['3'],    # Beta parameters
        ['7'],    # X parameters
        [str(pow(10, x)) for x in range(5, 11)],     # Number of samples
    ]
    args = list(product(*args))
    return args

def exec_profile(profiles, n: int):
    results = []
    for i in range(n):
        out = subprocess.Popen(
            ' '.join(profile),
            shell=True,
            stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT
        )
        stdout, stderr = out.communicate()
        key = [k for (k, v) in RUNNERS.items() if v == profile[0]]
        results.append((key[0], profile[-1], stdout))
    return results

def parse_result(runner: str, samples: str, out: str):
    out = out.decode('utf-8').split('\n')
    out = [
        o.split(' ')[-1]
        for o in out
    ]
    out = {
        'runner': runner,
        'samples': samples,
        'result': out[0],
        'time': out[1]
    }
    return out

def parse_results(runs):
    runs = [
        parse_result(*run)
        for run in runs
    ]
    results = [
        float(run['result'])
        for run in runs
    ]
    times = [
        float(run['time'])
        for run in runs
    ]
    out = {
        'runner': runs[0]['runner'],
        'samples': runs[0]['samples'],
        'result_avg': '{:.6f}'.format(round(mean(results), 6)),
        'result_std': '{:.6f}'.format(round(stdev(results), 6)),
        'time_avg': '{:.4f}'.format(round(mean(times), 4)),
        'time_std': '{:.4f}'.format(round(stdev(times), 4)),
    }
    return out

if __name__ == '__main__':
    profiles = get_profiles()
    with open('../results.csv', 'a+') as file:
        for i, profile in enumerate(profiles):
            out = exec_profile(profile, n = 10)
            out = parse_results(out)
            print(out)
            if i == 0:
                file.write(','.join(out.keys()) + '\n')
            file.write(','.join(out.values()) + '\n')
            file.flush()
