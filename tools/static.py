#!/usr/bin/env python3.7
import sys
import numpy as np
from PIL import Image
from math import floor, sqrt

if __name__ == '__main__':
    static = np.fromfile(sys.argv[1], dtype=int)
    n = floor(sqrt(static.shape[0]))
    static = static[:n*n]
    static = static.reshape((n, n))
    static = Image.fromarray(static * 255, mode='L').convert('1')
    static.show()
