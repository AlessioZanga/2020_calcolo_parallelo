#!/usr/bin/env python3.7
import sys
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    accepted = np.fromfile('./samples_accepted.bin', dtype=float)
    accepted = accepted.reshape((accepted.shape[0] // 2, 2))
    x, y = accepted.T
    plt.scatter(x, y, s = 1, c = '#03a9f4')
    counted = np.fromfile('./samples_counted.bin', dtype=float)
    counted = counted.reshape((counted.shape[0] // 2, 2))
    x, y = counted.T
    plt.scatter(x, y, s = 1, c = '#005cb2')
    plt.xlim(0, float(sys.argv[1]))
    plt.ylim(0, float(sys.argv[2]))
    plt.show()
