#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define SEED 0xB5AD4ECEDA1CE2A9

uint64_t init() {
    static uint64_t x = 0, w = 0;
    w += SEED;
    x *= x;
    x += w;
    x = (x >> 32) | (x << 32);
    return x;
}

double uniform(uint64_t *v) {
    uint64_t a = v[0];
    const uint64_t b = v[1];
    const uint64_t out = a + b;
    v[0] = b;
    a ^= a << 23;
    v[1] = a ^ b ^ (a >> 18) ^ (b >> 5);
    return (double)out / UINT64_MAX;
}

double gamma_pdf(double x, double alpha, double beta) {
    double out = 1;
    out *= pow(beta, -alpha);
    out *= pow(x, alpha - 1);
    out *= exp(-x / beta);
    for (double i = 2; i < alpha - 1; i++) out /= i;
    return out;
}

int main(int argc, char **argv) {
    FILE *f, *g;
    double x, y;
    uint64_t v[2];
    double accepted = 0, counted = 0;

    int alpha, beta;
    double X;

    alpha = atoi(argv[1]);
    beta = atoi(argv[2]);
    X = atof(argv[3]);

    f = fopen("samples_accepted.bin", "w");
    g = fopen("samples_counted.bin", "w");

    for (int i = 0; i < 2; i++) v[i] = init();

    for (double i = 0; i < atof(argv[4]); i++) {
        x = uniform(v) * 256;
        y = uniform(v);
        if (gamma_pdf(x, alpha, beta) > y) {
            if (x < X) {
                fwrite((void *) &x, 8, 1, g);
                fwrite((void *) &y, 8, 1, g);
                counted++;
            } else {
                fwrite((void *) &x, 8, 1, f);
                fwrite((void *) &y, 8, 1, f);
            }
            accepted++;
        }
    }

    fclose(f);
    fclose(g);

    return 0;
}
