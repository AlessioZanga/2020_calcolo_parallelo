# Esame di Calcolo Parallelo

## Introduzione

La seguente repository contiene l'implementazione di una soluzione per l'approssimazione della funzione di ripartizione di variabili aleatorie.

## Struttura della repository

Le implementazioni disponibili sono:

- Implementazione sequenziale (cartella "serial")
- Implementazione MPI
- Implementazione OpenMP
- Implementazione CUDA

La cartella comments contiene una relazione del lavoro svolto e una presentazione.

Nella cartella tools sono presenti alcuni strumenti:

- Script di profiler delle soluzione sopra citate
- Script di visualizzazione dei risultati in forma grafica
- Script di visualizzazione della distribuzione di sequenze pseudo-casuali
